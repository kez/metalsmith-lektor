const Metalsmith  = require('metalsmith');
const collections = require('@metalsmith/collections');
const layouts     = require('@metalsmith/layouts');
const markdown    = require('@metalsmith/markdown');
const permalinks  = require('@metalsmith/permalinks');
const lektor      = require('./lektor');

Metalsmith(__dirname)         // __dirname defined by node.js:
                              // name of the directory of this file
  .metadata({                 // add any variable you want
                              // use them in layout-files
    sitename: "My Static Site & Blog",
    siteurl: "https://example.com/",
    description: "It's about saying »Hello« to the world.",
    generatorname: "Metalsmith",
    generatorurl: "https://metalsmith.io/"
  })
  .source('./content')        // source directory
  .destination('./build')     // destination directory
  .clean(true)                // clean destination before
  .use(lektor({contentDir: '', modelsDir: './models'}))
  .build(function(err) {      // build process
    if (err) throw err;       // error handling is required
  });


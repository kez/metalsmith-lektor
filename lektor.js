'use strict';

const fs = require('fs');
const path = require('path');
const util = require('util')

const ini = require('ini')

const inspect = (...args) => print(util.inspect(...args, {showHidden: false, depth: null, colors: true}));
const print = (...args) => console.log(...args);

class Site {
    models = {};
    contents = {};

    constructor(models, contents) {
        // the `none` model is always present
        const mergedModels = Object.assign({}, models, {none: ''});
        Object.keys(mergedModels).forEach(modelName => {
            this.models[modelName] = ini.parse(mergedModels[modelName]);
        });

        Object.keys(contents).forEach(filepath => {
            this.contents[filepath] = this.parseContents(contents[filepath].contents.toString());
        });
        Object.keys(this.contents).forEach(filepath => {
            this.structureContents(this.contents[filepath], filepath);
        });
    }

    parseContents(contents) {
        let currentKey = null;
        let currentValue = '';
        let contentsObj = {};

        contents.split('\n').forEach(line => {
            if (line == '---') {
                contentsObj[currentKey] = currentValue.trim();
                currentKey = null;
                currentValue = '';
            } else if (currentKey == null && line.trim() != '') {
                let split = line.split(':');
                currentKey = split.shift().trim();
                currentValue = split.join(':');
            } else {
                currentValue += `\n${line}`;
            }
        });

        contentsObj[currentKey] = currentValue.trim();

        return contentsObj;
    }

    modelExists(modelName) {
        return Object.keys(this.models).includes(modelName);
    }

    getPageId(filepath) {
        return filepath.split(path.sep).slice(-1)[0];
    }

    getChildModelFromParent(filepath, contents) {
        let dirname = path.dirname(path.dirname(filepath)) + path.sep + 'contents.lr';
        if (dirname == '.') { return null; }

        let parentModel = this.getContentsModel(dirname, this.contents[dirname]);
        if (parentModel['children'] && parentModel['children']['model']) {
            return parentModel.children.model;
        } else {
            return null;
        }
    }

    getContentsModel(filepath, contents) {
        let modelName;
        if ('_model' in contents && !!contents['_model']) {
            return contents._model
        } else if ((modelName = this.getChildModelFromParent(filepath, contents))) {
            return modelName;
        } else if ((modelName = this.modelExists(this.getPageId(filepath)))) {
            return modelName;
        } else if (this.modelExists('page')) {
            return 'page';
        } else {
            return 'none';
        }
    }

    structureContents(contents, filepath) {
        contents._model = this.getContentsModel(filepath, contents);
    }
}

function lektorMetalsmith(options = {}) {
    const contentDir = options.contentDir;
    const modelsDir = options.modelsDir;

    let lektorModels = {};

    fs.readdir(modelsDir, (err, files) => {
        if (err) {
            throw err;
        }

        files.forEach(filepath => lektorModels[path.basename(filepath)] = fs.readFileSync([modelsDir, filepath].join(path.sep), 'utf8'));
    });

    return function (files, metalsmith) {
        let lektorSourceFiles = metalsmith.match('*/**/contents.lr');
        let contents = {};
        lektorSourceFiles.forEach(filepath => {
            // make sure the files are in the lektor content dir
            if (filepath.slice(0, contentDir.length) != contentDir) {
                // TODO: add logging here
                return;
            }

            contents[filepath.slice(contentDir.length)] = files[filepath];
        })

        let site = new Site(lektorModels, contents);
        inspect(site);
        // TODO: actually build the site
    }
}

module.exports = lektorMetalsmith;

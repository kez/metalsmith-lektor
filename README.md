# metalsmith-lektor

while researching static site generators for [TPA-RFC 37][], I stumbled across [metalsmith][]. Metalsmith stuck out to me because it just provides a list of filenames and buffers to javascript functions, it seemed extremely simple and flexible. For laughs, I wanted to see if I could feasibly re-implement a minimal subset of lektor in metalsmith. This is the result.

Mostly this repo is a proof-of-concept for a transitional tool. Moving away from lektor would be *extremely* difficult, because it would basically have to be all-or-nothing for each site, and that would take an immense amount of effort. Fully implementing lektor's API in metalsmith would *also* be a huge effort, so the goal here is to provide a "mostly compatible" subset of lektor we can use in the meantime, while slowly moving sites to use metalsmith. One of the benefits of a slow transition is that we can implement an i18n jinja function as a metalsmith plugin, and we can keep using it after moving away from metalsmith-lektor.

[TPA-RFC 37]: <https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-37-lektor-replacement>
[metalsmith]: <https://metalsmith.io>

## Progress

milestones, in no particular order

- [-] build a list of contents.lr files
	- mostly works, see below
- [-] resolve models for contents
	- also mostly works, see below
- [ ] render (simple) pages and their templates
- [ ] implement jinja functions
	- see below

### Known issues

- the glob used to match contents.lr files doesn't match the top-level `content/contents.lr` file. this also affects model resolution
- a large amount of lektor's functionality is provided by jinja functions. things like databags, site queries, etc. are implemented for jinja. most likely we'll never implement them all.

### Non-goals

- full jinja api compatibility: things like `site.query` can be hacked in without having to fully implement lektor's query system
- plugins: metalsmith provides its own plugin system, so we don't need lektor's event-based plugin system
